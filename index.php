<?php
/**
 * Dokumentacija ovog edukativnog projekta je generisana 2016-05-16
 * i nalazi se u direktorijumu doc/ sa pocetnom datotekom index.html.
 */
    require_once 'sys/Autoloader.php';

    Session::begin();

    $Request = substr($_SERVER['REQUEST_URI'], strlen(Configuration::PATH));

    $Routes = require_once 'Routes.php';


    $Arguments = [];
    $FoundRoute = null;
    foreach ($Routes as $Route) {
        if (preg_match($Route['Pattern'], $Request, $Arguments)) {
            $FoundRoute = $Route;
          
            
            break;
        }
    }


    unset($Arguments[0]);
    $Arguments = array_values($Arguments);

    require_once 'app/controllers/' . $FoundRoute['Controller'] . 'Controller.php';

    $imeKlase = $FoundRoute['Controller'] . 'Controller';

    $worker = new $imeKlase;

    if ($FoundRoute['Controller'] == "AdminC") {
        if (method_exists($worker, '__pre') ) {
            $worker->__pre();
        } 
    }
    
    if ( !method_exists($worker, $FoundRoute['Method']) ) {
        die('Ne postoji izabrani metod u izabranoj klasi!');
    }

    $imeMetoda = $FoundRoute['Method'];

    call_user_func_array([$worker, $imeMetoda], $Arguments);

    $DATA = $worker->getData();

    $viewLokacija = 'app/views/' . $FoundRoute['Controller'] . '/' . $FoundRoute['Method'] . '.php';
    require_once $viewLokacija;
    
