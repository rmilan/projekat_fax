//Skripa za postavljanje komentara
$("#send_msg").click(function(){
$('#username').removeClass("error");
    
var message      		= $("#message").val();
var video 				= $("#comment_id").val();
var url 				= $("#url_address").val();

if (validateMessage(message)) {
    $('#message').addClass('error');
    return false;
}

var dataString 		= 'message='+ message + '&video='+video;
                
$.ajax({
type: "POST",
url: url,
data: dataString,
cache: false,
success: function(result){
    if (result.indexOf("success") != -1) {
    	$(result).hide().prependTo("#message-list").fadeIn("slow");
    	$('#num').html(parseInt($('#num').html(), 10)+1);
    	$('#message').val('');
    	$('#message').removeClass('error');
    } else {
        var parts = result.split("^");

        for (var i = 0; i < parts.length; i++) {
            if (parts[i] == "message") {
                $('#message').addClass('error');   
            } 
        }
    }
}
});
return false;
});


// Scripta za proveru logina

$("#submit").click(function(){
    $('#username').removeClass("error");
    $('#email').removeClass("error");
    $('#password').removeClass("error");
    $('#re_password').removeClass("error");
    
var username      		= $("#username").val();
var email       		= $("#email").val();
var password         	= $("#password").val();
var re_password         = $("#re_password").val();
var url         		= $("#url_address").val();

if (!validateEmail(email)) {
    $('#email').addClass('error');
    return false;
}
if (!validatePassword(password)) {
    $('#password').addClass('error');
    $('#re_password').addClass('error');
    return false;
}
if (!validatePassword(re_password)) {
    $('#password').addClass('error');
    $('#re_password').addClass('error');
    return false;
}

var dataString 		= 'username='+ username + '&email='+ email + '&password='+ password + '&re_password='+ re_password;
                
$.ajax({
type: "POST",
url: url,
data: dataString,
cache: false,
success: function(result){
    if (result.indexOf("success") != -1) {
        window.location.href = "thanks";
    } else {
        var parts = result.split("^");

        for (var i = 0; i < parts.length; i++) {
            if (parts[i] == "username") {
                $('#username').addClass('error');   
            } else if (parts[i] == "email") {
                $('#email').addClass('error');
            } else if (parts[i] == "password") {
                $('#password').addClass('error');
                $('#re_password').addClass('error');
            }
        }
    }
}
});
return false;
});


// Skripta za registraciju korisnika

	$("#submit2").click(function(){
    $('#username').removeClass("error");
    $('#email').removeClass("error");
    $('#password').removeClass("error");
    $('#re_password').removeClass("error");
    
var username      		= $("#username").val();
var email       		= $("#email").val();
var password         	= $("#password").val();
var re_password         = $("#re_password").val();
var url2         		= $("#url_address").val();

if (!validateEmail(email)) {
    $('#email').addClass('error');
    return false;
}
if (!validatePassword(password)) {
    $('#password').addClass('error');
    $('#re_password').addClass('error');
    return false;
}
if (!validatePassword(re_password)) {
    $('#password').addClass('error');
    $('#re_password').addClass('error');
    return false;
}

var dataString 		= 'username='+ username + '&email='+ email + '&password='+ password + '&re_password='+ re_password;
                
$.ajax({
type: "POST",
url: url2,
data: dataString,
cache: false,
success: function(result){
    if (result.indexOf("success") != -1) {
        window.location.href = "thanks";
    } else {
        var parts = result.split("^");

        for (var i = 0; i < parts.length; i++) {
            if (parts[i] == "username") {
                $('#username').addClass('error');   
            } else if (parts[i] == "email") {
                $('#email').addClass('error');
            } else if (parts[i] == "password") {
                $('#password').addClass('error');
                $('#re_password').addClass('error');
            }
        }
    }
}
});
return false;
});


// Skripta za login

$("#login").click(function(){
$('#email2').removeClass("error");
$('#password2').removeClass("error");
    
var email               = $("#email2").val();
var password            = $("#password2").val();
var url3                = $("#url_adrs").val();

if (!validateEmail(email)) {
    $('#email2').addClass('error');
    return false;
}
if (!validatePassword(password)) {
    $('#password2').addClass('error');
    return false;
}

var dataString      = 'email='+ email + '&password='+ password;
                
$.ajax({
type: "POST",
url: url3,
data: dataString,
cache: false,
success: function(result){
    if (result.indexOf("success") != -1) {
        location.reload();
    } else {
        var parts = result.split("^");

        for (var i = 0; i < parts.length; i++) {
            if (parts[i] == "email2") {
                $('#email2').addClass('error');   
            } else if (parts[i] == "password2") {
                $('#password2').addClass('error');
            }
        }
    }
}
});
return false;
});

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validatePassword(password) {
  var re = /[a-zA-Z0-9]{5,16}$/;
  return re.test(password);
}

function validateMessage(message) {
    var msg = message;
  var n = msg.length;
    if (n < 5) {
        return true;
    } else if (n > 200) {
        return true;
    }
}