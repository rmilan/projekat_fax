<?php
    /**
     * Ova datoteka vraca niz asocijativnih nizova koji predstavljaju rute koje
     * postoje u ovoj aplikaciji.
     * <pre>
     * Svaka ruta je asocijativni niz koji mora da sadrzi indekse:
     *  - Pattern    - Regularni izraz koji treba da odgovara zahtevu da se ruta izvrsi
     *  - Controller - Ime kontrolera koji treba koristiti za odgovor zahtevu.
     *                 Ako je ime kontrolera Main, ime klase je IndexController.
     *                 Kao vrednost ovog indeksa asocijatvinog niza ide samo Main,
     *                 a ne IndexController kako je puno ime klase.
     *  - Method     - Ime metoda izabranog kontrolera koji treba izvrsiti za
     *                 odgovor na pristigli zahtev koji odgovara ovoj ruti.
     * </pre>
     * 
     * Primer:
     * <pre><code>
     * return [
     *   [
     *     'Pattern'    => '|^login/?$|',
     *     'Controller' => 'Index',
     *     'Method'     => 'login'
     *   ],
     *   [
     *     'Pattern'    => '|^logout/?$|',
     *     'Controller' => 'Index',
     *     'Method'     => 'logout'
     *   ],
     *   [ # Poslednja ruta koja ce se izvrsiti ako ni jedna pre ne odgovara pristiglom zahtevu.
     *     'Pattern'    => '|^.*$|',
     *     'Controller' => 'Index',
     *     'Method'     => 'index'
     *   ]
     * ];
     * <code></pre>
     */
    return [
        [
            'Pattern'       => '|admin/login/?$|',
            'Controller'    => 'LoginAdmin',
            'Method'        => 'login'
        ],
        [
            'Pattern'       => '|admin/logout/?$|',
            'Controller'    => 'LoginAdmin',
            'Method'        => 'logout'
        ],  
        [
            'Pattern'       => '|admin/login_chk/?$|',
            'Controller'    => 'LoginAdmin',
            'Method'        => 'check'
        ],
        [
            'Pattern'       => '|index/?$|',
            'Controller'    => 'Index',
            'Method'        => 'index'
        ],
        [
            'Pattern'       => '|admin/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'index'
        ],
        [
            'Pattern'       => '|admin/add/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'handle'
        ],
        [
            'Pattern'       => '|admin/login/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'login'
        ],
        [
            'Pattern'       => '|admin/login/check/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'check'
        ],
        [
            'Pattern'       => '|admin/videos/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'videos'
        ],
        [
            'Pattern'       => '|admin/videos/add_new/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'upload'
        ],
        [
            'Pattern'       => '|admin/upload_video/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'upload_video'
        ],
        [
            'Pattern'       => '|admin/users/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'users'
        ],
        [
            'Pattern'       => '|uploads/videos/[0-9_\.mp4]*/?$|',
            'Controller'    => 'Index',
            'Method'        => 'single'
        ],
        [
            'Pattern'       => '|single/([0-9]+)/?$|',
            'Controller'    => 'Index',
            'Method'        => 'single'
        ],
        [
            'Pattern'       => '|signup/?$|',
            'Controller'    => 'SignUp',
            'Method'        => 'index'
        ],
        [
            'Pattern'       => '|register_log/?$|',
            'Controller'    => 'SignUp',
            'Method'        => 'register_log'
        ],
        [
            'Pattern'       => '|login_log/?$|',
            'Controller'    => 'Login',
            'Method'        => 'login_log'
        ],
        [
            'Pattern'       => '|logout/?$|',
            'Controller'    => 'Login',
            'Method'        => 'logout'
        ],
        [
            'Pattern'       => '|([0-9]+)/?$|',
            'Controller'    => 'Index',
            'Method'        => 'category'
        ],
        [
            'Pattern'       => '|search/?$|',
            'Controller'    => 'Index',
            'Method'        => 'search'
        ],
        [
            'Pattern'       => '|send_msg/?$|',
            'Controller'    => 'Index',
            'Method'        => 'send_msg'
        ],
        [
            'Pattern'       => '|admin/edit/edit_category/?$|',
            'Controller'    => 'AdminC',
            'Method'        => 'edit_category'
        ],
        [ # Poslednja ruta za sve
            'Pattern'       => '|^.*$|',
            'Controller'    => 'Index',
            'Method'        => 'index'
        ]
    ];
