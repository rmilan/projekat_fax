<?php
    class AdminController extends Controller {
        public final function __pre() {
            if (!Session::exists('admin_id') or !is_numeric(Session::get('admin_id'))) {
                Misc::redirect('admin/login');
            }
        }
    }
