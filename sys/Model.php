<?php
    abstract class Model implements ModelInterface {
        protected final static function getTableName() {
        	return substr(strtolower(preg_replace('/([A-Z])/', '_$0', get_called_class())), 1, -6);
        }
        public final static function getAll() {
        	$tableName = self::getTableName();
            $SQL = 'SELECT * FROM ' . $tableName . 'ORDER BY id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        public final static function getById($id) {
        	$tableName = self::getTableName();
            $SQL = 'SELECT * FROM ' . $tableName . ' WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
    }
