<?php
    abstract class Controller {
        private $podaci = [];

        protected function set($name, $value) {
            if ( preg_match('/^[A-z]+[A-z0-9_]+$/', $name) ) {
                $this->podaci[$name] = $value;
            }
        }

        public function getData() {
            return $this->podaci;
        }

        public function index() {
            
        }
    }
