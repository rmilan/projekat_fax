-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2016 at 01:02 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `site_singitube`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_admin`
--

CREATE TABLE IF NOT EXISTS `site_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_admin`
--

INSERT INTO `site_admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '04FB872E88CA2B23BA97183DB631BEECF9DEB939D4E144E4F3D9D21F40B495808DAC5670AABEB1D213F98679415C3EF843EAAF85A587F84AA4894FDEFEF80D60');

-- --------------------------------------------------------

--
-- Table structure for table `site_categories`
--

CREATE TABLE IF NOT EXISTS `site_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `belong` int(11) NOT NULL,
  `icon` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_categories`
--

INSERT INTO `site_categories` (`id`, `cat_name`, `belong`, `icon`) VALUES
(1, 'Music', 0, 'glyphicon-music'),
(2, 'Sport', 0, 'glyphicon-king'),
(3, 'test kategorija', 0, 'glyphicon-lamp'),
(4, 'hahahah', 0, 'glyphicon-sunglasses'),
(5, 'test', 0, 'glyphicon-save-file'),
(6, 'samo test kategorija', 2, 'glyphicon-play'),
(8, 'aaaaa', 0, 'glyphicon-tags'),
(9, 'provera', 3, 'glyphicon-ok'),
(10, 'kategorija test 1', 5, 'glyphicon-euro'),
(11, 'test  icon ', 4, 'glyphicon-heart'),
(12, 'test icon 2', 2, 'glyphicon-barcode');

-- --------------------------------------------------------

--
-- Table structure for table `site_comments`
--

CREATE TABLE IF NOT EXISTS `site_comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  `video_id` int(11) NOT NULL,
  `post_date` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_comments`
--

INSERT INTO `site_comments` (`id`, `user_id`, `text`, `video_id`, `post_date`) VALUES
(1, 0, 'das das das dsa dsa sad sad sads ', 3, 1463507456),
(2, 1, 'ovo je samo test poruka ', 3, 1463507599),
(3, 1, 'provera da li radi ajax', 3, 1463508097),
(4, 1, 'provera da li radi ajax x2', 3, 1463508301),
(5, 1, 'provera da li radi ajax x3\\n', 3, 1463508371),
(6, 1, 'provera da li radi ajax x4\\n', 3, 1463508450),
(7, 1, 'provera da li radi ajax x4\\n', 3, 1463508455),
(8, 1, 'provera da li radi ajax x5\\n', 3, 1463508523),
(9, 1, 'sdfsdafasdfasd', 3, 1463508582),
(10, 1, 'sdfsdafasdfasd', 3, 1463508594),
(11, 1, 'swdfasfasdf', 3, 1463508601),
(12, 1, 'swdfasfassdddddd', 3, 1463508605),
(13, 1, 'asdasdasd', 3, 1463508629),
(14, 1, 'adasdasdasd', 3, 1463508689),
(15, 1, 'adasdasdasd', 3, 1463508691),
(16, 1, 'dfsdfdsdfsdfg', 3, 1463508807),
(17, 1, 'sdasdasd', 3, 1463508862),
(18, 1, 'sdasdasdsadasdasdas', 3, 1463508874),
(19, 1, 'dasdasdasd', 3, 1463509001),
(20, 1, 'dasdasdasd5555dfssssssss', 3, 1463509009),
(21, 1, 'test testa 33333', 3, 1463509111),
(22, 1, 'dfsdfsdfsdf', 3, 1463509225),
(23, 1, 'dfsdfsdfsdf sadasdasdas', 3, 1463509230),
(24, 1, 'asdasdas', 3, 1463509282),
(25, 1, 'asdasdassadasdasdas sadasd', 3, 1463509289),
(26, 1, 'dasdasd', 3, 1463509405),
(27, 1, 'dasdasddasdsad', 3, 1463509408),
(28, 1, 'dasdas das das das da', 3, 1463509563),
(29, 1, 'dasdas das das das da das das dad asdas', 3, 1463509566),
(30, 1, 'dasdas das das das da das das dad asdasd asd asd', 3, 1463509569),
(31, 1, 'dasdas das das das da das das dad asdasd asd asddas dsa dsa das', 3, 1463509574),
(32, 1, 'dasdas das das das da das das dad asdasd asd asddas dsa dsa dasdas das d', 3, 1463509579),
(33, 1, 'dasdas das das das da das das dad asdasd asd asddas dsa dsa dasdas das dfsdf sdf', 3, 1463509779),
(34, 1, 'das das asd s', 3, 1463509837),
(35, 1, 'das das asd sdas dsa das', 3, 1463509839),
(36, 1, 'das das asd sdas dsa dasdas das das as', 3, 1463509842),
(37, 1, 'das das asd sdas dsa dasdas das das asdas das das', 3, 1463509846),
(38, 1, 'fdsfzdfafd', 3, 1463509948),
(39, 1, 'fdsfzdfafddsadasdas', 3, 1463509950),
(40, 1, 'fdsfzdfafddsadasdasdasdsadsads', 3, 1463509951),
(41, 1, 'fdsfzdfafddsadasdasdasdsadsadssadsa dsa das das d', 3, 1463509954),
(42, 1, 'ovo radi konacno', 3, 1463510434),
(43, 1, 'ovo radi konacno 2222 ', 3, 1463510497),
(44, 1, 'sdsad adas dsa ', 3, 1463511006),
(45, 1, 'das das dsa dsa dsa d', 3, 1463511348),
(46, 1, 'rfasddfsa dsa dsa dsa ', 3, 1463511390),
(47, 1, 'dasd asd asd sa', 9, 1463511420),
(48, 1, 'dasd asdas dsa d', 3, 1463511918),
(49, 1, 'dasd asdas dsa ddas dsa d', 3, 1463511923),
(50, 1, 'fas dasd asd sa dsa', 3, 1463512005),
(51, 1, 'fasd as', 3, 1463512141),
(52, 1, 'fasd asda sd as', 3, 1463512146),
(53, 1, 'dsad sad asd', 3, 1463512168),
(54, 1, 'dsad sad asd dasd asd as', 3, 1463512170),
(55, 1, 'dasd asd as', 3, 1463512251),
(56, 1, 'dasd das dsa dsad sad sa dsad', 3, 1463512518),
(57, 1, 'dasd das dsa dsad sad sa dsaddsa ds adas ', 3, 1463512520),
(58, 1, 'dasd das dsa dsad sad sa dsaddsa ds adas d sad asd sa', 3, 1463512522),
(59, 1, 'dsa dsa dsa ', 9, 1463512529),
(60, 1, 'kaslhd kasd asd sald\\n', 6, 1463512782),
(61, 1, 'das dasd asd asd ', 6, 1463512833),
(62, 1, 'das das dsa dsa dsa dsa ', 6, 1463512863),
(63, 1, 'prvi komentar\\n', 7, 1463513097),
(64, 1, 'das das dasd ', 7, 1463513146);

-- --------------------------------------------------------

--
-- Table structure for table `site_users`
--

CREATE TABLE IF NOT EXISTS `site_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_users`
--

INSERT INTO `site_users` (`id`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@admin.com', '04fb872e88ca2b23ba97183db631beecf9deb939d4e144e4f3d9d21f40b495808dac5670aabeb1d213f98679415c3ef843eaaf85a587f84aa4894fdefef80d60');

-- --------------------------------------------------------

--
-- Table structure for table `site_videos`
--

CREATE TABLE IF NOT EXISTS `site_videos` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `category` int(11) NOT NULL,
  `youtube_id` varchar(100) NOT NULL,
  `date_created` int(11) NOT NULL,
  `tags` varchar(300) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `duration` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_videos`
--

INSERT INTO `site_videos` (`id`, `name`, `description`, `category`, `youtube_id`, `date_created`, `tags`, `file_path`, `image_name`, `duration`) VALUES
(1, 'Test 1 Name', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, '', 1463420138, 'tag1 music', '2788_1463420138.mp4', '2788_1463420138.png', ''),
(2, 'Test Name 2 ', ' long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 2, '', 1463420171, 'ha ha music', '7879_1463420171.mp4', '7879_1463420171.png', ''),
(3, 'Test Name 3', ' long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 3, '', 1463420217, 'mcadssdasds', '5994_1463420217.mp4', '5994_1463420217.png', ''),
(5, 'Test Name 4', ' long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 4, '', 1463420361, 'dfasdfa sd asd sa', '5140_1463420361.mp4', '5140_1463420361.png', ''),
(6, 'Test Name 5', 'das das dsa dsa das das dsa dsa dkjsan dkjasnd ksadl askjdn skajn dkj sn fadkjasnfj dsnakfn dakjnf kjdanf kjdsankfjn dskjfndskj nfsdj nkjfdsan kjfndsakjf ndskj fnasdkjnf kjdsna kfndaskjfn adksj nfkjdsna kdnkjf ndakjfnkjdsanf kjdsnaf kjndskjf ndsknf kedf  long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 5, '', 1463420582, 'asf adf dsf dsf d', '5688_1463420582.mp4', '5688_1463420582.png', ''),
(7, 'Test Name 6', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 3, '', 1463423127, 'das dsd asdsa d', '3809_1463423127.mp4', '3809_1463423127.png', ''),
(8, 'Test Name 8', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 3, '', 1463423142, 'das dasd asd asd', '3723_1463423142.mp4', '3723_1463423142.png', ''),
(9, 'Test Name 9', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 3, '', 1463423483, 'das dasd asd asd', '5326_1463423483.mp4', '5326_1463423483.png', ''),
(10, 'test duration', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 3, '', 1463434288, 'dasd asd asd as', '9572_1463434288.mp4', '9572_1463434288.png', '00:00:36.88');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_admin`
--
ALTER TABLE `site_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_categories`
--
ALTER TABLE `site_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_comments`
--
ALTER TABLE `site_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_users`
--
ALTER TABLE `site_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_videos`
--
ALTER TABLE `site_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `site_admin`
--
ALTER TABLE `site_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_categories`
--
ALTER TABLE `site_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `site_comments`
--
ALTER TABLE `site_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `site_users`
--
ALTER TABLE `site_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_videos`
--
ALTER TABLE `site_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
