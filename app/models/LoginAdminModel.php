<?php
    /**
     * Model koji odgovara tabeli site_admin
     */
    class LoginAdminModel implements ModelInterface {
        /**
         * Metod koji vraca spisak svih admin korisnika
         * @return stdClass
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_admin` ORDER BY `id`;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca admin korisnika odredjen po id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_admin` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji radi prveru za logovanje na admin panel
         * @param string $user
         * @param string $pass
         * @return stdClass
         */
        public static function check($user, $pass) {
            $SQL = 'SELECT id FROM site_admin WHERE username = ? AND password = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user, $pass]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
    }
