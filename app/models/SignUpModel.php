<?php
    /**
     * Model koji odgovara tabeli site_users
     */ 
    class SignUpModel implements ModelInterface {
        /**
         * Metod koji vraca sve korisnike iz baze
         * @return type
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_users` ORDER BY `id`;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca korisnika odredjen po id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_users` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji radi proveru da li postoji mejl u bazi
         * @param string $email
         * @return int
         */
        public static function check($email) {
            $SQL = 'SELECT id FROM site_users WHERE email = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$email]);
            $count = $prep->rowCount();
            return $count;
        }
        /**
         * Metod koji radi unos podataka korisnika u bazu
         * @param string $username
         * @param string $email
         * @param string $pass
         * @return boolean
         */
         public static function insert($username, $email, $pass) {
            $SQL = 'INSERT INTO `site_users` (`username`, `email`, `password`) VALUES (?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute( [ $username, $email, $pass ] );
        }
    }
