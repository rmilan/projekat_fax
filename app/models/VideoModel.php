<?php
    /**
     * Model koji odgovara tabeli site_videos
     */ 
    class VideoModel implements ModelInterface {
        /**
         * Metod koji vraca sve video snimke iz baze
         * @return stdClass
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_videos` ORDER BY `id`;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca poslednja tri video snimka
         * @return stdClass
         */
        public static function getAllLatest() {
            $SQL = 'SELECT * FROM `site_videos` ORDER BY `id` DESC LIMIT 3;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca poslednjih 16 video snimaka
         * @return stdClass
         */
        public static function getTop16() {
            $SQL = 'SELECT * FROM `site_videos` ORDER BY `id` DESC LIMIT 16;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca 8 naizmencno izabranih video snimaka
         * @return stdClass
         */
        public static function rand8() {
            $SQL = 'SELECT * FROM `site_videos` ORDER BY RAND() LIMIT 8;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca sve informacije po odredjenom id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_videos` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji radi pretragu po zadatom parametru
         * @param string $content
         * @return stdClass
         */
        public static function search($content) {
            $SQL = 'SELECT * FROM `site_videos` WHERE name LIKE ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$content]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca sve podatke iz odredjene kategorije
         * @param int $id
         * @return stdClass
         */
        public static function getAllById($id) {
            $SQL = 'SELECT * FROM `site_videos` WHERE category = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji upisuje podatke u bazu
         * @param string $name
         * @param string $description
         * @param int $category
         * @param int $youtube_id
         * @param int $date_created
         * @param string $tags
         * @param string $filename_to_save
         * @param string $image_name
         * @param string $duration
         * @return boolean
         */
        public static function insert($name, $description, $category, $youtube_id, $date_created, $tags, $filename_to_save, $image_name, $duration) {
            $SQL = 'INSERT INTO `site_videos` (`name`, `description`, `category`, `youtube_id`, `date_created`, `tags`, `file_path`, `image_name`, `duration`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute( [ $name, $description, $category, $youtube_id, $date_created, $tags, $filename_to_save, $image_name, $duration] );
        }
        /**
         * Metod koji vraca naziv kategorije po odredjenm id-u
         * @param int $id
         * @return stdClass
         */
        public static function catName($id) {
            $SQL = 'SELECT * FROM `site_categories` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca podatke po odredjenoj kategoriji i limitom od 12
         * @param int $id
         * @return stdClass
         */
        public static function upnext($id) {
            $SQL = 'SELECT * FROM `site_videos` WHERE category = ? LIMIT 12;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
    }
