<?php
    /**
     * Model koji odgovara tabeli site_users
     */ 
    class UserModel implements ModelInterface {
        /**
         * Metod koji vraca sve korisnike iz baze
         * @return stdClass
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_users` ORDER BY `id`;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca korisnika odredjen po id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT username FROM `site_users` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
    }
