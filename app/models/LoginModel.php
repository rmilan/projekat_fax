<?php
    /**
     * Model koji odgovara tabeli site_users
     */ 
    class LoginModel implements ModelInterface {
        /**
         * Metod koji vraca spisak svih registrovanih korisnika
         * @return stdClass
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_users` ORDER BY `id`;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca korisnika odredjen po id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_users` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji radi prveru za logovanje
         * @param string $email
         * @param string $pass
         * @return int
         */
        public static function check($email, $pass) {
            $SQL = 'SELECT id FROM site_users WHERE email = ? AND password = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$email, $pass]);
            $count = $prep->rowCount();
            return $count;
        }
        /**
         * Metod koji vraca vrednost id-a prema odgovarajucem mejlu
         * @param string $email
         * @return stdClass
         */
        public static function returnId($email) {
            $SQL = 'SELECT id FROM site_users WHERE email = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$email]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
    }
