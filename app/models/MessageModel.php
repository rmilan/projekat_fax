<?php
    /**
     * Model koji odgovara tabeli site_users
     */ 
    class MessageModel implements ModelInterface {
        /**
         * Metod koji vraca sve poruke iz baze
         * @return stdClass
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_comments` ORDER BY `datetime` DESC;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca poruku odredjenu po id-u
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_comments` WHERE message_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca spisak poruka vezane za odredjeni video
         * @param int $id
         * @return stdClass
         */
        public static function getAllById($id) {
            $SQL = 'SELECT * FROM `site_comments` WHERE video_id = ? ORDER BY id DESC;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji radi unos poruke u bazu
         * @param int $user_id
         * @param string $text
         * @param int $video
         * @param int $post_date
         * @return boolean
         */
        public static function insert($user_id, $text, $video, $post_date) {
            $SQL = 'INSERT INTO `site_comments` (`user_id`, `text`, `video_id`, `post_date`) VALUES (?, ?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute( [ $user_id, $text, $video, $post_date ] );
        }
        /**
         * Metod koji prebrojava koliko je poruka vezano za odredjeni video snimak
         * @param int $id
         * @return int
         */
        public static function count($id) {
            $SQL = 'SELECT id FROM site_comments WHERE video_id = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            $count = $prep->rowCount();
            return $count;
        }
    }
