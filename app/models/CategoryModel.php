<?php
    /**
     * Model koji odgovara tabeli site_categories
     */
    class CategoryModel implements ModelInterface {
        /**
         * Metod koji vraca spisak svih kategorija poredjanih po id-u
         * @return array
         */
        public static function getAll() {
            $SQL = 'SELECT * FROM `site_categories` ORDER BY id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca spisak svih kategorija u kojima postoji neki video snimak poredjanih po id-u
         * @return array
         */
        public static function sidebar() {
            $SQL = 'SELECT DISTINCT site_categories.id, site_categories.cat_name, site_categories.icon FROM `site_categories` WHERE EXISTS (SELECT * FROM `site_videos` WHERE site_videos.category = site_categories.id);';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji vraca objekat sa podacima kategorije ciji je id dat kao argument
         * @param int $id
         * @return stdClass
         */
        public static function getById($id) {
            $SQL = 'SELECT * FROM `site_categories` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        /**
         * Metod koji dodaje kategoriju sa podacima naziva kategorije, pripadanje drugoj i iconu za prikazivanje
         * @param int $category
         * @param int $belong
         * @param string $icon
         * @return boolean
         */
        public static function insert($category, $belong, $icon) {
            $SQL = 'INSERT INTO `site_categories` (`cat_name`, `belong`, `icon`) VALUES (?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute( [ $category, $belong, $icon] );
        }
        /**
         * Metod koji vraca ime kategorije po zadatom id-u
         * @param int $id
         * @return stdClass
         */
        public static function catName($id) {
            $SQL = 'SELECT * FROM `site_categories` WHERE id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
    }
