<?php
    /**
    * Klasa kontrolera za admin panel
    */
    class AdminCController extends AdminController {
        /**
        * Indeks metod admin kontrolera za rad sa kategrijama
        */
        public function index() {
            $this->set('categories', CategoryModel::getAll());
        }
        /**
        * User metod admin kontrolera za rad sa korisnicima
        */
        public function users() {
            $this->set('users', UserModel::getAll());
        }
        /**
        * Videos metod admin kontrolera za rad sa video snimcima
        */
        public function videos() {
            $this->set('videos', VideoModel::getAll());
        }
        /**
        * Upload metod admin kontrolera - forma za dodavanje
        */
        public function upload() {
            $this->set('categories', CategoryModel::getAll());
        }
        /**
        * Upload_video metod admin kontrolera - logika za dodavanje video snimaka
        */
        public function upload_video() {
            if ($_POST) {
                $name               = filter_input(INPUT_POST, 'name');
                $description        = filter_input(INPUT_POST, 'description');
                $category           = filter_input(INPUT_POST, 'category');
                $youtube_id         = filter_input(INPUT_POST, 'youtube_id');
                $tags               = filter_input(INPUT_POST, 'tags');

                $allowed_extensions = array("mp4");
                $filename_parts = explode(".", $_FILES['video']['name']);
                $file_extension = $filename_parts[count($filename_parts)-1];
                if (!in_array($file_extension, $allowed_extensions)) die("extension not allowed");
                $random_name = rand(1, 9999)."_".time();
                $filename_to_save = $random_name.".".$file_extension;
                move_uploaded_file($_FILES['video']['tmp_name'], "assets/uploads/videos/".$filename_to_save);

                $date_created = time();
                
                //video dir
                $video = "G:/xampp/htdocs/projekti/projekat_fax/assets/uploads/videos/".$filename_to_save;
                //where to save the image
                $image = "G:/xampp/htdocs/projekti/projekat_fax/assets/uploads/images/".$random_name.".png";
                //time to take screenshot at
                $interval = '00:00:12';
                //screenshot size
                $size = '535x346';
                $ffmpeg = Configuration::BASE."assets/ffmpeg/bin/ffmpeg";
                echo "Starting ffmpeg...\n\n<br>";
                $rand_time = rand(1,5);
                //$cmd = "ffmpeg -i $video -ss $interval -f image2 -s $size -vframes 1 $image"; 
                exec ("ffmpeg -i  $video -ss 00:00:$rand_time.435 -vframes 1 $image 2>&1");
                $d = "";
                $cmd = "ffmpeg -i  $video -ss 00:00:$rand_time.435 -s 320x240 -vframes 1 $image 2>&1";
                $fp = popen($cmd . ' 2>&1', 'r');
                while (!feof($fp)) $d .= fread($fp, 1024);
                fclose($fp);
                $duration = "";
                $cmd2 = "ffprobe -i $video -show_entries format=duration -v quiet -of csv='p=0'";
                $fp = popen($cmd2, 'r');
                while (!feof($fp)) $duration .= fread($fp, 1024);
                fclose($fp);
                $r = explode('Duration: ', $d);
                if (isset($r[1])){
                    $r = explode(',', $r[1]);
                    $duration = $r[0];
                }
                //ffmpeg -i  G:/xampp/htdocs/projekti/projekat_fax/uploads/videos/4984_1459756875.mp4 -ss 00:00:03.435 -vframes 1 G:/xampp/htdocs/projekti/projekat_fax/uploads/images/test.png
                //echo shell_exec($cmd);
                //echo "Done.\n";
                
                $image_name = $random_name.".png";

                $rezultat = VideoModel::insert($name, $description, $category, $youtube_id, $date_created, $tags, $filename_to_save, $image_name, $duration);
                print_r($rezultat);
                if ($rezultat) {
                    header("Location:".Configuration::BASE."admin/videos");
                } else {
                    header("Location:".Configuration::BASE."admin/videos/prazan_rezultat");
                }
            } else {
                header("Location:".Configuration::BASE."admin/videos/ne_postoji_post");
            }
        }
        /**
        * Hanlde metod admin kontrolera - logika za dodavanje kategorija
        */
        public function handle() {
            if ($_POST['category'] != "") {
                $category       = filter_input(INPUT_POST, 'category');
                $belong         = filter_input(INPUT_POST, 'belong');
                $icon          = filter_input(INPUT_POST, 'icon');

                $rezultat = CategoryModel::insert($category, $belong, $icon);
                if ($rezultat) {
                    header("Location:".Configuration::BASE."admin");
                } else {
                    header("Location:".Configuration::BASE."admin");
                }
            } else {
                header("Location:".Configuration::BASE."admin");
            }
        }
        /**
        * Edit_category metod admin kontrolera za editovanje kategorija
        */
        public function edit_category() {
        }
    }
