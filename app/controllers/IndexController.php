<?php
/**
 * Klasa kontrolera za prikaz videa
 */
    class IndexController extends Controller {
        /**
     * Indeks metod za vracanje, kategorija i video snimaka
     */
        public function index() {
            $this->set('categories', CategoryModel::sidebar());
            $this->set('videos', VideoModel::getAllLatest());
            $this->set('top16', VideoModel::getTop16());
            $this->set('rand8', VideoModel::rand8());
        }
        /**
     * Single metod za vracanje posebnih stranica
     */
        public function single($id) {
            $this->set('categories', CategoryModel::sidebar());
            $this->set('details', VideoModel::getById($id));
            $this->set('messages', MessageModel::getAllById($id));
            $this->set('count', MessageModel::count($id));
        }
        /**
     * Category metod za vracanje kategorija
     */
        public function category($id) {
            $this->set('categories', CategoryModel::sidebar());
            $this->set('category_name', VideoModel::catName($id));
            $this->set('category', VideoModel::getAllById($id));
        }
        /**
     * Search metod za pretragu video snimaka
     */
        public function search($content) {
            $search_text = filter_input(INPUT_POST, 'search');
            $search_data = "%".$search_text."%";
            $search_results =  VideoModel::search($search_data);

            $this->set('categories', CategoryModel::sidebar());
            $this->set('search_results', $search_results);
        }
        /**
     * send_msg metod za komentare
     */
        public function send_msg() {
        $message        = filter_input(INPUT_POST, 'message');
        $video          = filter_input(INPUT_POST, 'video');
        $user_id        = $_SESSION['user_id'];

        $have_error         = false;
        $error              = "";

        if (strlen($message)<1){
            $error      = $error."message";
            $have_error = true;
        }
        if (strlen($video)<1){
            $error      = $error."^message";
            $have_error = true;
        }
        if (strlen($user_id)<1){
            $error      = $error."^message";
            $have_error = true;
        }
        if ($have_error == true){
            die("$error");
        }

        $message_e          = mysql_real_escape_string($message);
        $video_e            = mysql_real_escape_string($video);
        $user_id_e          = mysql_real_escape_string($user_id);
        $post_date          = mysql_real_escape_string(time());

        MessageModel::insert($user_id_e, $message_e, $video_e, $post_date);

        $username = UserModel::getById($user_id);
        $time_now = date("Y-m-d h:i:a", $post_date);

        echo "<div class=\"media\">
                <!--success-->
                <h5>$username->username</h5>
                <div class=\"media-left\">
                    <a href=\"#\">
                        
                    </a>
                </div>
                <div class=\"media-body\">
                    <p>$message</p>
                    <span>Published on : $time_now</span>
                </div>
            </div>";
                }
    }
