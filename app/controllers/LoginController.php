<?php
    /**
    * Klasa kontrolera za logovanje korisnika
    */
    class LoginController extends Controller {
        /**
        * Login_log metod je logika za proveru korisnika pri logvanju i njegovo logovanje
        */
    	public function login_log() {
    		
            $email              = filter_input(INPUT_POST, 'email');
            $password           = filter_input(INPUT_POST, 'password');

            $have_error         = false;
            $error              = "";
            $clean_email        = filter_var($email,FILTER_SANITIZE_EMAIL);


            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error      = $error."^email2";
                $have_error = true;
            }
            if ($clean_email != $email){
                $error      = $error."^email2";
                $have_error = true;
            }
            if (strlen($password)<5){
                $error      = $error."^password2";
                $have_error = true;
            }
            if ($have_error == true){
                die("$error");
            }

            $email_e            = mysql_real_escape_string($email);
            $encrypt_password   = mysql_real_escape_string(hash('sha512', $password.Configuration::PASSWORD_SALT));

            if (LoginModel::check($email_e, $encrypt_password) > 0 ) {
                $user_id = LoginModel::returnId($email);
                Session::set('user_id', $user_id->id);
                echo "success";
            } else {
                $error = "email2^password2";
                die("$error");
            }

    	}
        /**
        * Check metod proverava da li su ispravni podaci i loguje korisnika
        */
        public function check() {
            if ($_POST['user'] != "" && $_POST['password'] !="") {
                $user    		= filter_input(INPUT_POST, 'user');
                $pass    		= filter_input(INPUT_POST, 'password');

                $pass_sha  		= hash('sha512', $pass.Configuration::PASSWORD_SALT);

                $rezultat = LoginAdminModel::check($user, $pass_sha);
                if ($rezultat and @$rezultat->id) {
                	Session::set('admin_id', $rezultat->id);
	                Session::set('is_admin', 'yesss');
	                Misc::redirect('admin');
                } else {
                	header("Location:".Configuration::BASE."admin/login");
                	$this->set('error', 'error');
                }
            } else {
                header("Location:".Configuration::BASE."admin/login");
            }
        }
        /**
        * Logout metod odjavljuje korisnika
        */
        public function logout() {
	        Session::end();
	        Misc::redirect('');
	    }
    }
