<?php
    /**
    * Klasa kontrolera za logovanje na admin panel
    */
    class LoginAdminController extends Controller {
        /**
        * Login metod admin panla
        */
    	public function login() {
    		
    	}
        /**
        * Check metod radi proveru da li taj administrator postoji u bazi
        */
        public function check() {
            if ($_POST['user'] != "" && $_POST['password'] !="") {
                $user    		= filter_input(INPUT_POST, 'user');
                $pass    		= filter_input(INPUT_POST, 'password');

                $pass_sha  		= hash('sha512', $pass.Configuration::PASSWORD_SALT);

                $rezultat = LoginAdminModel::check($user, $pass_sha);
                if ($rezultat and @$rezultat->id) {
                	Session::set('admin_id', $rezultat->id);
	                Session::set('is_admin', 'yesss');
	                Misc::redirect('admin');
                } else {
                	header("Location:".Configuration::BASE."admin/login");
                	$this->set('error', 'error');
                }
            } else {
                header("Location:".Configuration::BASE."admin/login");
            }
        }
        /**
        * Logout metod odjavljuje korisnika sa admin panel-a
        */
        public function logout() {
	        Session::end();
	        Misc::redirect('admin/login');
	    }
    }
