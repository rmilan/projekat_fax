<?php
	/**
    * Klasa kontrolera za registraciju korisnika
    */
    class SignUpController extends Controller {
    	/**
        * Index metod je forma za registraciju korisnika
        */
        public function index() {
        }
        /**
        * Register_log metod je logika za proveru podataka i upisa u bazu
        */
        public function register_log() {

        $username           = filter_input(INPUT_POST, 'username');
        $email              = filter_input(INPUT_POST, 'email');
        $password           = filter_input(INPUT_POST, 'password');
        $re_password        = filter_input(INPUT_POST, 're_password');

        $have_error         = false;
        $error              = "";
        $clean_email        = filter_var($email,FILTER_SANITIZE_EMAIL);
        $check_email        = SignUpModel::check($email);


        if (strlen($username)<2) {
            $error      = $error."^username";
            $have_error = true;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error      = $error."^email";
            $have_error = true;
        }
        if ($clean_email != $email){
            $error      = $error."^email";
            $have_error = true;
        }
        if ($check_email > 0){
            $error      = $error."^email";
            $have_error = true;
        }
        if (strlen($password)<5){
            $error      = $error."^password";
            $have_error = true;
        }
        if ($password != $re_password) {
            $error      = $error."^password";
            $have_error = true;
        }
        if ($have_error == true){
            die("$error");
        }

        $username_e         = mysql_real_escape_string($username);
        $email_e            = mysql_real_escape_string($email);
        $encrypt_password   = mysql_real_escape_string(hash('sha512', $password.Configuration::PASSWORD_SALT));

        SignUpModel::insert($username_e, $email_e, $encrypt_password);


        echo "success";
        }
    }
