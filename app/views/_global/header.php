<!DOCTYPE HTML>
<html>
    <head>
        <title>Projekti rad</title>

        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--<link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link href="<?php echo Configuration::BASE; ?>assets/css/dashboard.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/style.css" rel='stylesheet' type='text/css' media="all" />
        <script src="<?php echo Configuration::BASE; ?>assets/js/jquery-1.11.1.min.js"></script>
    </head>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo Configuration::BASE; ?>"><img src="<?php echo Configuration::BASE; ?>assets/img/SingiTube-logo.png" alt="" width="200"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="top-search">
                <form class="navbar-form navbar-right" action="<?php echo Configuration::BASE; ?>search" method="post">
                    <input type="text" class="form-control" name="search" placeholder="Search...">
                    <input type="submit" value=" ">
                </form>
            </div>
            <div class="header-top-right">
                <div class="file">
                    <a href="<?php echo Configuration::BASE; ?>upload">Upload</a>
                </div>
                <?php if(!isset($_SESSION['user_id'])):?>  
                <div class="signin">
                    <a href="<?php echo Configuration::BASE; ?>signup" class="play-icon">Sign Up</a>
                    <!-- pop-up-box -->
                    <script type="text/javascript" src="<?php echo Configuration::BASE; ?>assets/js/modernizr.custom.min.js"></script>    
                    <link href="<?php echo Configuration::BASE; ?>assets/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" property='stylesheet' />
                    <script src="<?php echo Configuration::BASE; ?>assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
                    <!--//pop-up-box -->  
                </div>
                <script>
                    $(document).ready(function() {
                        $('.popup-with-zoom-anim').magnificPopup({
                            type: 'inline',
                            fixedContentPos: false,
                            fixedBgPos: true,
                            overflowY: 'auto',
                            closeBtnInside: true,
                            preloader: false,
                            midClick: true,
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in'
                        });                                                         
                    });
                </script>
                <input type="hidden" name="url_adrs" id="url_adrs" value="<?php echo Configuration::BASE.'login_log'?>">
                <div class="signin">
                    <a href="#small-dialog" class="play-icon popup-with-zoom-anim">Sign In</a>
                    <div id="small-dialog" class="mfp-hide">
                        <h3>Login</h3>
                        <div class="social-sits">
                            <div class="facebook-button">
                                <a href="#">Connect with Facebook</a>
                            </div>
                            <div class="chrome-button">
                                <a href="#">Connect with Google</a>
                            </div>
                            <div class="button-bottom">
                                <p>New account? <a href="#small-dialog2" class="play-icon popup-with-zoom-anim">Signup</a></p>
                            </div>
                        </div>
                        <div class="signup">
                            <form>
                                <input type="text" class="email" placeholder="Enter email"  name="email" id="email2" pattern="[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}" title="Enter correct email" required />
                                <input type="password" placeholder="Password" name="password" id="password2" pattern="^[A-Z0-9._%+-]{6,254}$" title="Enter valid password" required />
                                <input type="submit" id="login" value="LOGIN"/>
                            </form>
                            <div class="forgot">
                                <a href="#">Forgot password ?</a>
                            </div>
                        </div>
                        <?php else:?>
                        <div class="signin">
                            <a href="<?php Configuration::BASE?>logout" class="play-icon">Logout</a>
                        </div>
                        <?php endif;?>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </nav>