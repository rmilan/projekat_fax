<div class="col-sm-3 col-md-2 sidebar">
<div class="top-navigation">
	<div class="t-menu">MENU</div>
	<div class="t-img">
		<img src="assets/img/lines.png" alt="" />
	</div>
	<div class="clearfix"> </div>
</div>
	<div class="drop-navigation drop-navigation">
	  <ul class="nav nav-sidebar">
	  	<li id="home"><a href="<?=Configuration::BASE;?>" class="home-icon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
	  	<?php foreach ( $DATA['categories'] as $category ): ?>
	  		<li id="<?=$category->id?>"><a href="<?php echo Configuration::BASE.$category->id?>" class="user-icon"><span class="glyphicon <?=$category->icon?>" aria-hidden="true"></span> <?php echo $category->cat_name; ?></a></li>
	  	<?php endforeach;?>
		<li><a href="history.html" class="sub-icon"><span class="glyphicon glyphicon-home glyphicon-hourglass" aria-hidden="true"></span>History</a></li>
	  </ul>
		<div class="side-bottom">
			<div class="side-bottom-icons">
				<ul class="nav2">
					<li><a href="#" class="facebook"> </a></li>
					<li><a href="#" class="facebook twitter"> </a></li>
					<li><a href="#" class="facebook chrome"> </a></li>
					<li><a href="#" class="facebook dribbble"> </a></li>
				</ul>
			</div>
			<div class="copyright">
				<p>Copyright © 2016 SingiTube. All Rights Reserved | Design by Milan :D</p>
			</div>
		</div>
	</div>
</div>