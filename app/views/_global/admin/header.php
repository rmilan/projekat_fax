<!DOCTYPE HTML>
<html>
    <head>
        <title>Admin Panel</title>
        <link href="<?php echo Configuration::BASE; ?>assets/css/admin.css" rel='stylesheet' type='text/css' media="all" />
        <!--<link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap-iconpicker.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/dashboard.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/style.css" rel='stylesheet' type='text/css' media="all" />
        <script src="<?php echo Configuration::BASE; ?>assets/js/jquery-1.11.1.min.js"></script>
    </head>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo Configuration::BASE; ?>admin"><img src="<?php echo Configuration::BASE; ?>assets/img/SingiTube-logo-admin.png" alt="" width="200"/></a>
        </div>
        <div class="pull-right">
          <a id="logout" class="navbar-brand" href="<?php echo Configuration::BASE; ?>admin/logout"><img src="<?php echo Configuration::BASE; ?>assets/img/Logout.png" alt="" width="30" style="display: inline; margin: 0 5px;"/>Logout</a>
        </div>
        <div class="clearfix"> </div>
      </div>
    </nav>
