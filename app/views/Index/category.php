<?php include_once 'app/views/_global/header.php'; ?>
<?php include_once 'app/views/_global/sidebar.php'; ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<div class="main-grids">
	<div class="recommended">
		<div class="recommended-grids">
			<div class="recommended-info">
				<h3><?=$DATA['category_name']->cat_name;?></h3>
			</div>
			<?php
			if(empty($DATA['category'])) echo "<p style='font-size:18pt;'>No videos added to this catogory yet!</p><style>.footer {position:fixed;width:100%;bottom: 0px;}</style>";
			$num = 0;
			foreach($DATA['category'] as $videos):?>
			<div class="col-md-3 resent-grid recommended-grid">
				<div class="resent-grid-img recommended-grid-img">
					<a href="<?=Configuration::BASE."single/".$videos->id?>"><img src="<?=Configuration::BASE?>assets/uploads/images/<?=$videos->image_name?>" height="213" alt="" /></a>
					<div class="time small-time">
						<p><i class="glyphicon glyphicon-time"></i> <?= $videos->duration;?></p>
					</div>
				</div>
				<div class="resent-grid-info recommended-grid-info video-info-grid">
					<h5><a href="<?=Configuration::BASE."single/".$videos->id?>" class="title"><?=$videos->name?></a></h5>
					<ul>
						<li><p class="author author-info"><a href="#" class="author">Admin</a></p></li>
						<li class="right-list"><p class="views views-info">2,114,200 views</p></li>
					</ul>
				</div>
			</div>
			<?php 
			$num++;
			if($num == 4){
				echo "<div class=\"clearfix\"> </div></div><div class=\"recommended-grids\">";
				$num = 0;
			} 
			endforeach; ?>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<?php include_once 'app/views/_global/footer.php'; ?>
<script>
$( document ).ready(function() {
    var id = <?=$DATA['category_name']->id?>;
	$('#'+id).addClass('active');
});
</script>