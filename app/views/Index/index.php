<?php include_once 'app/views/_global/header.php'; ?>
<?php include_once 'app/views/_global/sidebar.php'; ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<div class="main-grids">
	<div class="top-grids">
		<div class="recommended-info">
			<h3>Recent Videos</h3>
		</div>
		<?php foreach($DATA['videos'] as $videos): ?>
		<div class="col-md-4 resent-grid recommended-grid slider-top-grids">
			<div class="resent-grid-img recommended-grid-img">
				<a href="<?=Configuration::BASE."single/".$videos->id?>"><img src="<?=Configuration::BASE?>assets/uploads/images/<?=$videos->image_name?>" alt="" height="300"/></a>
				<div class="time">
					<p><i class="glyphicon glyphicon-time"></i> <?= $videos->duration;?></p>
				</div>
			</div>
			<div class="resent-grid-info recommended-grid-info">
				<h3><a href="<?=Configuration::BASE."single/".$videos->id?>" class="title title-info"><?=$videos->name;?></a></h3>
				<ul>
					<li><p class="author author-info"><a href="#" class="author">Admin</a></p></li>
					<li class="right-list"><p class="views views-info">2,114,200 views</p></li>
				</ul>
			</div>
		</div>
		<?php endforeach; ?>
		<div class="clearfix"> </div>
	</div>
	<div class="recommended">
		<div class="recommended-grids">
			<div class="recommended-info">
				<h3>Top 15</h3>
			</div>
			<script src="<?php echo Configuration::BASE; ?>assets/js/responsiveslides.min.js"></script>
			<script src="<?php echo Configuration::BASE; ?>assets/js/script.js"></script>
			<div  id="top" class="callbacks_container">
				<ul class="rslides" id="slider3">
					<li>
						<div class="animated-grids">
							<?php
							$num = 0; 
							foreach($DATA['top16'] as $videos):?>
							<div class="col-md-3 resent-grid recommended-grid slider-first">
								<div class="resent-grid-img recommended-grid-img">
									<a href="<?=Configuration::BASE."single/".$videos->id?>"><img src="<?=Configuration::BASE?>assets/uploads/images/<?=$videos->image_name?>" height="20" alt="" /></a>
									<div class="time small-time slider-time">
										<p><i class="glyphicon glyphicon-time"></i> <?= $videos->duration;?></p>
									</div>
								</div>
								<div class="resent-grid-info recommended-grid-info">
									<h5><a href="<?=Configuration::BASE."single/".$videos->id?>" class="title"><?=$videos->name?></a></h5>
									<div class="slid-bottom-grids">
										<div class="slid-bottom-grid">
											<p class="author author-info"><a href="#" class="author">Admin</a></p>
										</div>
										<div class="slid-bottom-grid slid-bottom-right">
											<p class="views views-info">2,114,200 views</p>
										</div>
										<div class="clearfix"> </div>
									</div>
								</div>
							</div>
							<?php
							$num++;
							if($num == 4){
								echo "<div class=\"clearfix\"> </div></div></li><li><div class=\"animated-grids\">";
								$num = 0;
							} 
							endforeach; 
							?>
							<div class="clearfix"> </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="recommended">
		<div class="recommended-grids">
			<div class="recommended-info">
				<h3>Recommended</h3>
			</div>
			<?php 
			$num = 0;
			foreach($DATA['rand8'] as $videos):?>
			<div class="col-md-3 resent-grid recommended-grid">
				<div class="resent-grid-img recommended-grid-img">
					<a href="<?=Configuration::BASE."single/".$videos->id?>"><img src="<?=Configuration::BASE?>assets/uploads/images/<?=$videos->image_name?>" height="213" alt="" /></a>
					<div class="time small-time">
						<p><i class="glyphicon glyphicon-time"></i> <?= $videos->duration;?></p>
					</div>
				</div>
				<div class="resent-grid-info recommended-grid-info video-info-grid">
					<h5><a href="<?=Configuration::BASE."single/".$videos->id?>" class="title"><?=$videos->name?></a></h5>
					<ul>
						<li><p class="author author-info"><a href="#" class="author">Admin</a></p></li>
						<li class="right-list"><p class="views views-info">2,114,200 views</p></li>
					</ul>
				</div>
			</div>
			<?php 
			$num++;
			if($num == 4){
				echo "<div class=\"clearfix\"> </div></div><div class=\"recommended-grids\">";
				$num = 0;
			} 
			endforeach; ?>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<?php include_once 'app/views/_global/footer.php'; ?>
<script>
$( document ).ready(function() {
	$('#home').addClass('active');
});
// You can also use "$(window).load(function() {"
$(function () {
  // Slideshow 4
  $("#slider3").responsiveSlides({
	auto: true,
	pager: false,
	nav: true,
	speed: 500,
	namespace: "callbacks",
	before: function () {
	  $('.events').append("<li>before event fired.</li>");
	},
	after: function () {
	  $('.events').append("<li>after event fired.</li>");
	}
  });

});
</script>