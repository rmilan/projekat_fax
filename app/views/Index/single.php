<?php include_once 'app/views/_global/header.php'; ?>
<?php include_once 'app/views/_global/sidebar.php'; ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="show-top-grids">
            <div class="col-sm-8 single-left">
                    <div class="song">
                            <div class="song-info">
                                    <h3><?php echo htmlspecialchars($DATA['details']->name); ?></h3>	
                    </div>
                            <div class="video-grid">
                                    <iframe src="<?=Configuration::BASE."assets/uploads/videos/".$DATA['details']->file_path?>" allowfullscreen></iframe>
                            </div>
                    </div>
                    <div class="song-grid-right">
                            <div class="share">
                                    <h5>Share this</h5>
                                    <ul>
                                            <li><a href="#" class="icon fb-icon">Facebook</a></li>
                                            <li><a href="#" class="icon dribbble-icon">Dribbble</a></li>
                                            <li><a href="#" class="icon twitter-icon">Twitter</a></li>
                                            <li><a href="#" class="icon pinterest-icon">Pinterest</a></li>
                                            <li><a href="#" class="icon whatsapp-icon">Whatsapp</a></li>
                                            <li><a href="#" class="icon like">Like</a></li>
                                            <li><a href="#" class="icon comment-icon">Comments</a></li>
                                            <li class="view">200 Views</li>
                                    </ul>
                            </div>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="published">
                                    <div class="load_more">	
                                            <ul>
                                                    <li>
                                                            <h4>Published on 	<?php echo htmlspecialchars(date("d M Y",$DATA['details']->date_created)); ?></h4>
                                                            <p><?php echo htmlspecialchars($DATA['details']->description); ?></p>
                                                            <div class="load-grids">
                                                                    <div class="load-grid">
                                                                            <p>Category</p>
                                                                    </div>
                                                                    <div class="load-grid">
                                                                            <a href="<?=Configuration::BASE.$DATA['details']->category?>"><?=VideoModel::catName($DATA['details']->category)->cat_name?></a>
                                                                    </div>
                                                                    <div class="clearfix"> </div>
                                                            </div>
                                                    </li>
                                            </ul>
                                    </div>
                    </div>
                    <div class="all-comments">
                            <div class="all-comments-info">
                                    <a href="#">All Comments (<i id="num"><?=$DATA['count']?></i>)</a>
                                    <div class="box">
                                            <form>
                                                    <textarea placeholder="Message" id="message"></textarea>
                                                    <input type="submit" value="SEND" id="send_msg">
                                                    <div class="clearfix"> </div>
                                            </form>
                                    </div>
                            </div>
                            <div class="media-grids" id="message-list">
                                    <?php foreach($DATA['messages'] as $msg): ?>
                                    <div class="media">
                                            <h5><?=UserModel::getById($msg->user_id)->username;?></h5>
                                            <div class="media-left">
                                                    <a href="#">

                                                    </a>
                                            </div>
                                            <div class="media-body">
                                                    <p><?=$msg->text;?></p>
                                                    <span>Published on :<?=date("Y-m-d h:i:a", $msg->post_date);?></span>
                                            </div>
                                    </div>
                                    <?php endforeach; ?>
                            </div>
                    </div>
            </div>
            <div class="col-md-4 single-right">
                    <h3>Up Next</h3>
                    <div class="single-grid-right">
                            <?php 
                            $up_data = VideoModel::upnext($DATA['details']->category);
                            foreach($up_data as $upnext): 
                                    if ($upnext->id == $DATA['details']->id) continue;
                            ?>
                            <div class="single-right-grids">
                                    <div class="col-md-4 single-right-grid-left">
                                            <a href="<?php echo Configuration::BASE; ?>single/<?=$upnext->id?>"><img src="<?php echo Configuration::BASE; ?>assets/uploads/images/<?=$upnext->image_name?>" height="85" alt="" /></a>
                                    </div>
                                    <div class="col-md-8 single-right-grid-right">
                                            <a href="single.html" class="title"><?=$upnext->name?></a>
                                            <p class="author"><a href="#" class="author">Admin</a></p>
                                            <p class="views">2,114,200 views</p>
                                    </div>
                                    <div class="clearfix"> </div>
                            </div>
                            <?php endforeach; ?>
                    </div>
            </div>
            <div class="clearfix"> </div>
    </div>
    <input type="hidden" name="comment_id" id="comment_id" value="<?=$DATA['details']->id?>">
    <input type="hidden" name="url_address" id="url_address" value="<?php echo Configuration::BASE.'send_msg'?>">
<?php include_once 'app/views/_global/footer.php'; ?>