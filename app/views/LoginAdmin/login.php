<!DOCTYPE HTML>
<html>
    <head>
        <title>Admin Login</title>
<?php
if (Session::exists('admin_id')) {
        Misc::redirect('admin');
    }
?>
<link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />
<link href="<?php echo Configuration::BASE; ?>assets/css/admin.css" rel='stylesheet' type='text/css' media="all" />
<script src="<?php echo Configuration::BASE; ?>assets/js/jquery-1.11.1.min.js"></script>
    </head>
<div class="container">    
        
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3"> 
        
        <div class="row">          
            <img src="../assets/img/SingiTube-logo-admin.png" alt="" width="350" style="display: block; margin-left: auto; margin-right: auto;"/>
        </div>
        
        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-title text-center">SingiTube Admin Panel Login</div>
            </div>     

            <div class="panel-body" >

                <form action="<?php echo Configuration::BASE; ?>admin/login_chk" name="form" id="form" class="form-horizontal" enctype="multipart/form-data" method="POST">
                   
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="user" type="text" class="form-control" name="user" value="" placeholder="User">                                        
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>                                                                  

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-sm-12 controls">
                            <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-log-in"></i> Log in</button>                          
                        </div>
                    </div>

                </form>     
            </div>                     
        </div>  
    </div>
</div>