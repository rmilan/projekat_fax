<?php include_once 'app/views/_global/admin/header.php'; ?>
<?php include_once 'app/views/_global/admin/sidebar.php'; ?>
<div class="container" style="padding-top:5%; padding-left:5%;">
  <h2>All Users</h2>
  	<br>

  <table class="table table-hover tabela">
    <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    	<?php foreach ( $DATA['users'] as $user ): ?>
    		<tr>
                    <td><?php echo $user->username; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td>Edit</td>
                    <td>Delete</td>
                </tr>
    	<?php endforeach;?>
    </tbody>
  </table>
</div>
<script>
    $('#users').addClass('active');
</script>