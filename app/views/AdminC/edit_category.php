<?php include_once 'app/views/_global/admin/header.php'; ?>
<?php include_once 'app/views/_global/admin/sidebar.php'; ?>
<div class="container" style="padding-top:5%; padding-left:5%;">
  <h2>All Categories</h2>
  	<br>
	  	<div class="col-md-12 marginTop">
	    	<h5 class="col-md-2">Add New Category</h5>
	    	<form method="post" action="<?php echo Configuration::BASE; ?>admin/add">
				<div class="col-md-4">
			    	<input class="form-control" type="text" name="category"/>
			    </div>
		        <div class="col-md-4">
			        <select class="form-control" name="belong">
			        	<option value="0">none</option>
			        	<?php foreach ( $DATA['categories'] as $category ): ?>
			            <option value="<?=$category->id?>"><?php echo $category->cat_name; ?></option>
			            <?php endforeach;?>
			        </select>
		        </div>
		        <button class="btn btn-default" role="iconpicker" name="icon"></button>
		         <input type="submit" value="Add" class="btn btn-primary"/>
	         </form>
	    </div>
	    <br><br><br><br><br><br>

  <table class="table table-hover tabela">
    <thead>
      <tr>
        <th>Category Name</th>
        <th>Belong to Category</th>
        <th>Icon</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    	<?php foreach ( $DATA['categories'] as $category ): ?>
    	<?php
    		$cat = CategoryModel::catName($category->belong);
    	?>
    		<tr>
		        <td><?php echo $category->cat_name; ?></td>
		        <td><?php echo (!isset($cat->cat_name) ? "none" : $cat->cat_name); ?></td>
		        <td><i class="glyphicon <?php echo $category->icon; ?>"></i></td>
		        <td><a href="<?php echo Configuration::BASE; ?>admin/edit-category">Edit</td>
		        <td>Delete</td>
		    </tr>
    	<?php endforeach;?>
    </tbody>
  </table>
</div>
	<script src="<?php echo Configuration::BASE; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo Configuration::BASE; ?>assets/js/iconset-glyphicon.min.js"></script>
    <script src="<?php echo Configuration::BASE; ?>assets/js/bootstrap-iconpicker.min.js"></script>
<script>
    $('#categories').addClass('active');
</script>