<?php include_once 'app/views/_global/admin/header.php'; ?>
<?php include_once 'app/views/_global/admin/sidebar.php'; ?>
<div class="container" style="padding-top:5%; padding-left:5%;">
  <h2>All Videos</h2>
  	<br>
	  	<div class="col-md-12 marginTop">
	    	<h5 class="col-md-2">Add New Video</h5>
	    		<a class="btn btn-primary" href="<?php echo Configuration::BASE; ?>admin/videos/add_new">Add</a>  
	    </div>
	    <br><br><br><br><br><br>

  <table class="table table-hover tabela">
    <thead>
      <tr>
        <th>Video Name</th>
        <th>Category</th>
        <th>YouTube ID</th>
        <th>Duration</th>
        <th>Date</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    	<?php foreach ( $DATA['videos'] as $video ): ?>
    	<?php
    		$cat = VideoModel::catName($video->category);
    	?>
    		<tr>
		        <td><?php echo $video->name; ?></td>
		        <td><?php echo (!isset($cat->cat_name) ? "none" : $cat->cat_name); ?></td>
            <td><?php echo $video->youtube_id; ?></td>
		        <td><?php echo $video->duration; ?></td>
		        <td><?php echo date("Y-m-d h:i a" ,$video->date_created); ?></td>
		        <td>Edit</td>
		        <td>Delete</td>
		    </tr>
    	<?php endforeach;?>
    </tbody>
  </table>
</div>
<script>
    $('#videos').addClass('active');
</script>