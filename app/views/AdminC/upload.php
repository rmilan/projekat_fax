<?php include_once 'app/views/_global/admin/header.php'; ?>
<?php include_once 'app/views/_global/admin/sidebar.php'; ?>
<div class="container" style="padding-top:5%; padding-left:5%;">
      <div class="upload_video">
        <h2>Upload</h2>
        <form action="<?php echo Configuration::BASE; ?>admin/upload_video" method="post" enctype="multipart/form-data"> 
          <label>Video Name</label>
          <input class="form-control" type="text" name="name"><br>
          <label>Description</label>
          <textarea class="form-control" name="description" ></textarea><br>
          <label>Category</label>
          <select class="form-control" name="category">
            <option value="0">none</option>
            <?php foreach ( $DATA['categories'] as $category ): ?>
              <option value="<?=$category->id?>"><?php echo $category->cat_name; ?></option>
              <?php endforeach;?>
          </select><br>
          <label>YouTube ID</label>
          <input class="form-control" type="text" name="youtube_id" ><br>
          <label>Tags</label>
          <input class="form-control" type="text" name="tags" ><br>
          <label>Select Video</label>
          <input type="file" name="video" id="video">
          <input  type="submit" value="Upload" class="btn btn-primary pull-right">
          <div class="clearfix"></div>
        </form>
    </div>
</div>
<script>
    $('#videos').addClass('active');
</script>
