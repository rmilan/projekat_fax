<?php
/**
 * Ova klasa predstavlja centralno mesto gde se nalaze svi vazni konfiguracioni
 * parametri ove veb aplikacije, koji su definisani kao clanovi podaci konstante.
 */
    final class Configuration {
        const DB_HOST = 'localhost';
        const DB_USER = 'root';
        const DB_PASS = '';
        const DB_NAME = 'site_singitube';

        const BASE = 'http://localhost/projekti/projekat_fax/';
        const PATH = 'projekti/projekat_fax/';


        const PASSWORD_SALT = "kljfiuwhdciudfi83tr32bdqw0923uihd8";
    }
